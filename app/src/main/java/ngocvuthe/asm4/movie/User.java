package ngocvuthe.asm4.movie;

import java.io.Serializable;

public class User implements Serializable {

    private String id;
    private String user_name;
    private String email;
    private String gender;
    private String image_url;
    private String birthday;

    public User(String id, String user_name, String email, String gender, String image_url, String birthday) {
        this.id = id;
        this.user_name = user_name;
        this.email = email;
        this.gender = gender;
        this.image_url = image_url;
        this.birthday = birthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
