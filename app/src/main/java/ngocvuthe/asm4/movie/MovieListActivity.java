package ngocvuthe.asm4.movie;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieListActivity extends AppCompatActivity {

    ImageButton userInfor;
    GridView movieListView;
    private MovieAdapter movieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        showAllMovieList();
        showUserImage();
        logout();
    }

    public void showAllMovieList() {
        movieListView = findViewById(R.id.movieList);
        Bundle data = getIntent().getExtras();

        final List<Movie> movieList = (List<Movie>) data.getSerializable("MovieList");
        movieAdapter = new MovieAdapter(movieList, this);
        movieListView.setAdapter(movieAdapter);

        movieListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    public void showUserImage() {
        userInfor = findViewById(R.id.userInfo);
        Bundle data = getIntent().getExtras();
        final User user = (User) data.getSerializable("User");
        Log.d("User", user.getImage_url());
        Picasso.with(this)
            .load(user.getImage_url())
            .into(userInfor);

        userInfor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MovieListActivity.this, UserInfoActivity.class);
                intent.putExtra("User", user);
                startActivity(intent);
            }
        });
    }

    public void logout() {
        userInfor = findViewById(R.id.userInfo);

        userInfor.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final AlertDialog.Builder adb = new AlertDialog.Builder(MovieListActivity.this);
                adb.setTitle("Logout?");
                adb.setMessage("Are you sure you want to logout ?");
                adb.setNegativeButton("Cancel", null);
                adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        LoginManager.getInstance().logOut();
                        Intent intent = new Intent(MovieListActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();                   }
                });
                adb.show();
                return true;
            }
        });
    }
}
