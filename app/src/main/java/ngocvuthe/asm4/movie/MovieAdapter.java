package ngocvuthe.asm4.movie;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieAdapter extends BaseAdapter {

    private List<Movie> movieList;
    private Context context;

    public MovieAdapter(List<Movie> movieList, Context context) {
        this.movieList = movieList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return movieList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.movie_item, null);

            holder = new ViewHolder();
            holder.moviePicture =  convertView.findViewById(R.id.moviePicture);
            holder.movieName =  convertView.findViewById(R.id.movieName);
            holder.moviePrice =  convertView.findViewById(R.id.moviePrice);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(context)
                .load(movieList.get(position).getImage())
                .into(holder.moviePicture);

        holder.movieName.setText(movieList.get(position).getTitle());

        holder.movieName.setTextColor(Color.parseColor("#FA0000"));

        holder.moviePrice.setText(movieList.get(position).getPrice());

        holder.moviePrice.setTextColor(Color.parseColor("#FA0000"));

        return convertView;
    }

    private static class ViewHolder {
        public ImageView moviePicture;
        public TextView movieName;
        public TextView moviePrice;
    }
}
