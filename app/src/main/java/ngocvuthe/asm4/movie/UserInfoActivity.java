package ngocvuthe.asm4.movie;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class UserInfoActivity extends AppCompatActivity {

    ImageView userAvatar;
    TextView userName;
    TextView userEmail;
    TextView userBirthday;
    TextView userGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_infor);
        showUser();
    }

    public void showUser() {
        Bundle data = getIntent().getExtras();
        final User user = (User) data.getSerializable("User");

        userAvatar = findViewById(R.id.userPicture);

        userName = findViewById(R.id.userName);

        userEmail = findViewById(R.id.userEmail);

        userBirthday = findViewById(R.id.userBirthday);

        userGender = findViewById(R.id.userGender);

        Picasso.with(this)
                .load(user.getImage_url())
                .into(userAvatar);

        userName.setText(user.getUser_name());

        userEmail.setText(user.getEmail());

        userBirthday.setText(user.getBirthday());

        userGender.setText(user.getGender());
    }

}
