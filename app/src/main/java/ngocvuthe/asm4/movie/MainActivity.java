package ngocvuthe.asm4.movie;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private User user;
    String id, name, email, gender, birthday, profile_pic;
    Button facebookLogin, googleLogin;
    public List<Movie> movieList;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_main);
        callbackManager = CallbackManager.Factory.create();
        onClickLoginFacebook();
    }

    public void getAllMovieFromServer() {
        OkHttpClient client = new OkHttpClient();
        Moshi moshi = new Moshi.Builder().build();

        Type movieType = Types.newParameterizedType(List.class, Movie.class);
        final JsonAdapter<List<Movie>> jsonAdapter = moshi.adapter(movieType);

        Request request = new Request.Builder()
                .url("https://api.androidhive.info/json/movies_2017.json")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Error", "Network Error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                movieList = jsonAdapter.fromJson(json);
            }
        });
    }

    public void setFacebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getData();
                    }

                    @Override
                    public void onCancel() {
                        System.out.println("onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {

                    }
                });
    }

    public void getData() {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object,
                                            GraphResponse response) {
                        try {
                            if (object.has("id")) {
                                id = object.getString("id");
                            }

                            if (object.has("name")) {
                                name = object.getString("name");
                            }
                            if (object.has("email")) {
                                email = object.getString("email");
                            }

                            if (object.has("gender")) {
                                gender = object.getString("gender");
                            }

                            if (object.has("birthday")) {
                                birthday = object.getString("birthday");
                            }

                            profile_pic = "https://graph.facebook.com/" + id + "/picture?type=large";

                            user = new User(id, name, email, gender, profile_pic, birthday);

                            Intent intent = new Intent(MainActivity.this, MovieListActivity.class);
                            intent.putExtra("User", user);
                            intent.putExtra("MovieList", (Serializable) movieList);
                            setResult(MovieListActivity.RESULT_OK, intent);
                            startActivity(intent);

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onClickLoginFacebook() {
        getAllMovieFromServer();

        facebookLogin = findViewById(R.id.facebook_login_button);

        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFacebookLogin();
            }
        });
    }

    public void onClickLoginGoogle() {
        googleLogin = findViewById(R.id.google_login_button);
        googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onStart() {
//        LoginManager.getInstance().logOut();
        super.onStart();
    }
}
